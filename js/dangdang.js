var positiondiv = 0;
var time;
var posi = 3;
var on;
var pr = 1;
var right;
$(document).ready(function(){
	$(".top-pic .close1").click(function(){
		$(this).parent().remove();
		$(this).remove();
	});
	$(".qrcode .close").click(function(){
		$(".qrcode .weima").remove();
		$(this).remove();
	});
	$(".middle .top-list").mousemove(function(){
		$(".middle .top-list").removeClass("top-listactive");
		$(this).addClass("top-listactive");
		$(".top-list .top-list-font").removeClass("top-list-fontactive");
		$(this).find(".top-list-font").addClass("top-list-fontactive");
		var position = $(".middle .top-list").index($(this));
		$(this).parent().parent().find($(".middle-bottom .list")).hide();
		$(this).parent().parent().find($(".middle-bottom .list")).eq(position).show();

	});
	$(".bottom-top-list").mousemove(function(){
		$(".bottom-top-list").removeClass("top-listactive");
		$(this).addClass("top-listactive");	
		$(".bottom-top-list .top-list-font").removeClass("top-list-fontactive");
		$(this).find(".top-list-font").addClass("top-list-fontactive");
		var position = $(".bottom-top-list").index($(this));
		$(".bottom-top-list").parent().parent().find($(".bottom-img a")).hide();
		$(".bottom-top-list").parent().parent().find($(".bottom-img a")).eq(position).show();
	});
	$(".bottom-box").mouseenter(function(){
		$(".bottom-direction-left").animate({left: '0px',opacity:'0.5'}, "slow");
		$(".bottom-direction-right").animate({right: '0px',opacity:'0.5'}, "slow");
		clearInterval(time);
	});
	$(".bottom-box").mouseleave(function(){
		$(".bottom-direction-left").animate({left: '-30px'}, "slow");
		$(".bottom-direction-right").animate({right: '-30px'}, "slow");
		time = setInterval(setswichimage,2000);
	});
	time = setInterval(setswichimage,2000);
	function setswichimage(){
		$(".top-box-img .top-box-link").hide();
		$(".top-box-img .top-box-link").eq(positiondiv).show();
		$(".bottom-box-first").hide();
		$(".bottom-box-first").eq(positiondiv).show();
		$(".number .number-list").removeClass("number-list_active");
		$(".number .number-list").eq(positiondiv).addClass("number-list_active");
		positiondiv = (positiondiv+1)%$(".number-list").size();
	};
	$(".top-box").mouseenter(function(){
		$(".direction-left").animate({left: '0px',opacity:'0.5'}, "slow");
		$(".direction-right").animate({right: '0px',opacity:'0.5'}, "slow");
		clearInterval(time);
	});
	$(".top-box").mouseleave(function(){
		$(".direction-left").animate({left: '-30px'}, "slow");
		$(".direction-right").animate({right: '-30px'}, "slow");
		time = setInterval(setswichimage,2000);
	});
	$(".direction-right").click(function(){
		setswichimage();
	});
	$(".direction-left").click(function(){
		positiondiv = (positiondiv-2)%$(".number-list").size();
		setswichimage();
	});
	$(".number-list").mouseenter(function(){
		clearInterval(time);
		var pos = $(".number-list").index(this);
		positiondiv = pos;
		setswichimage();
	});
	$(".number-list").mouseleave(function(){
		clearInterval(time);
	});
	$(".bottom-direction-left").click(function(){
		positiondiv = (positiondiv-2)%$(".number-list").size();
		setswichimage();
	});
	$(".bottom-direction-right").click(function(){
		setswichimage();
	});
	$(".bottom-box-link").mousemove(function(){
		$(this).find($(".cover")).css("display","none");
	});
	$(".bottom-box-link").mouseout(function(){
		$(this).find($(".cover")).css("display","block");
	});
	$(".screen-bottom-right a img").mouseenter(function(){
		$(this).animate({"left":"-5px"});
	});
	$(".screen-bottom-right a img").mouseleave(function(){
		$(this).animate({"left":"0px"},10);
	});
	on = setInterval(setswichimageleft,3000);
	function setswichimageleft(){
		$(".screen-bottom-left").hide();
		$(".screen-bottom-left").eq(posi).show();
		$(".two-screen-left .on").removeClass("on_active");
		$(".two-screen-left .on").eq(posi).addClass("on_active");
		posi = (posi-1)%$(".two-screen-left .on").size();
	};
	$(".two-screen-left .on").mousemove(function(){
		clearInterval(on);
		var pos = $(".two-screen-left .on").index(this);
		posi = pos;
		setswichimageleft();
	});
	$(".two-screen-left .on").mouseout(function(){
		on = setInterval(setswichimageleft,3000);
	});
	$(".screen-bottom-left").mouseenter(function(){
		clearInterval(on);
	});
	$(".screen-bottom-left").mouseleave(function(){
		on = setInterval(setswichimageleft,3000);
	});
	right = setInterval(setswichimageright,3000);
	function setswichimageright(){
		$(".screen-bottom-right").hide();
		$(".screen-bottom-right").eq(pr).show();
		$(".two-screen-right .on").removeClass("on_active");
		$(".two-screen-right .on").eq(pr).addClass("on_active");
		pr = (pr-1)%$(".two-screen-right .on").size();
	};
	$(".two-screen-right .on").mousemove(function(){
		clearInterval(right);
		var pos = $(".two-screen-right .on").index(this);
		pr = pos;
		setswichimageright();
	});
	$(".two-screen-right .on").mouseout(function(){
		right = setInterval(setswichimageright,3000);
	});
	$(".screen-bottom-right").mouseenter(function(){
		clearInterval(right);
	});
	$(".screen-bottom-right").mouseleave(function(){
		right = setInterval(setswichimageright,3000);
	});
});